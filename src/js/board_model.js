class Board {
    constructor(columns, rows) {
        this._columns = columns
        this._rows = rows
        this._map = this.createEmptyMap()
        this._players = [new Player('Luciano','player1'), new Player('Hudson','player2')]
        this._currentPlayer = this._players[0]
    }

    get columns() {
        return this._columns
    }

    set columns(_) {
        throw 'Cant change this value'
    }

    get rows() {
        return this._rows
    }

    set rows(_) {
        throw 'Cant change this value'
    }

    get map() {
        return this._map
    }

    set map(_) {
        throw 'Cant change this value'
    }

    get currentPlayer() {
        return this._currentPlayer
    }

    set currentPlayer(_) {
        throw 'Cant change this value'
    }

    createEmptyMap() {
        let map = []
        for (let i = 0; i < this.rows; i++) {
            map.push(new Array(this.columns))
        }
        return map
    }

    renderMap(container) {
        container.innerText = ''
        
        for(let j = 0; j < this.columns; j++) {
            const column = document.createElement('div')
            column.classList.add('column')
            column.style.width = `${100/this.columns}%`
            column.dataset.column = j
            column.addEventListener('click', () => this.handleClick(j))
            
            for(let i = 0; i < this.rows; i++) {
                const cell = document.createElement('div')
                cell.classList.add('cell')
                cell.style.height = `${100/this.rows}%`
                cell.dataset.row = i
                column.appendChild(cell)
            }
            container.appendChild(column)
        }
    }

    resetBoard(container) {
        this._map = this.createEmptyMap()
        this.renderMap(container)
    }

    switchPlayer() {
        const currentPlayerPos = this._players.indexOf(this.currentPlayer)
        const newPos = (currentPlayerPos + 1)%this._players.length
        this._currentPlayer = this._players[newPos]
    }

    handleClick(column) {
        let row = this.map.findIndex(row => row[column] !== undefined)
        if(row === -1) {
            row = this.rows
        }
        if(row === 0) {
            return undefined
        }

        this.map[row - 1][column] = this.currentPlayer

        const cell = new Cell(column, row, this.currentPlayer.className)
        cell.render()
        if(this.isWinnableMove(column,row-1)) {
            console.log(`${this.currentPlayer.name} ganhou`)
        }
        this.switchPlayer()
    }

    checkHorizontal(column, row) {
        const start = (column - 3) >= 0 ? (column - 3) : 0
        const end = (column + 3) <= this.columns ? (column + 3) : this.columns
        let counter = 0
        for(let j = start; j <= end; j++) {
            if(this.map[row][j] === this.currentPlayer) {
                counter++
            } else {
                counter = 0
            }
            if (counter === 4) {
                return true
            }
        }
        return false
    }

    checkVertical(column, row) {
        const start = row
        const end = (row + 3) < this.rows ? (row + 3) : this.rows - 1
        let counter = 0
        for (let i = start; i <= end; i++) {
            if(this.map[i][column] === this.currentPlayer) {
                counter++
            } else {
                counter = 0
            }
            if (counter === 4) {
                return true
            }
        }
        return false
    }

    isWinnableMove(column, row) {
        return this.checkHorizontal(column, row) || this.checkVertical(column, row)
    }
}