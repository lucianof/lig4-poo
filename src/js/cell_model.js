class Cell {
    constructor(posX, posY, className) {
        this.className = className
        this._x = posX
        this._y = posY
    }

    get x() {
        return this._x
    }

    set x(_) {
        throw 'Cant change this value'
    }

    get y() {
        return this._y
    }

    set y(_) {
        throw 'Cant change this value'
    }


    render() {
        const div = document.querySelector(`.column[data-column="${this.x}"] > .cell[data-row="${this.y - 1}"]`)
        div.innerText = ''

        const playerDiv = document.createElement('div')
        playerDiv.classList.add(this.className)
        div.appendChild(playerDiv)
    }
}