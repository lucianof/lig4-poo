const test_checkHorizontal = () => {
    const player1 = new Player('Luciano','player1')
    const player2 = new Player('Hudson','player2')
    
    const tests = [
        {
            played: {
                map: [
                    [player1, player1, player1, player1, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0],
                ],
                column: 2,
                row: 0,
                currentPlayer: player1
            },
            expected: true
        },
        {
            played: {
                map: [
                    [0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0],
                    [player1, player1, player1, player1, 0, 0, 0],
                ],
                column: 0,
                row: 5,
                currentPlayer: player1
            },
            expected: true
        },
        {
            played: {
                map: [
                    [0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0],
                    [0, 0, player2, player1, player1, player1, player1],
                ],
                column: 6,
                row: 5,
                currentPlayer: player1
            },
            expected: true
        },
        {
            played: {
                map: [
                    [0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0],
                    [player1, player1, player2, player1, 0, 0, 0],
                ],
                column: 1,
                row: 5,
                currentPlayer: player1
            },
            expected: false
        }
        
    ]
    const board = new Board(6,7)

    tests.forEach(test => {
        board._map = test.played.map
        board._currentPlayer = test.played.currentPlayer
        console.assert(board.checkHorizontal(test.played.column, test.played.row) === test.expected, {
            function: "checkHorizontal",
            test: {...test, got: board.checkHorizontal(test.played.column, test.played.row)}
        })
    })
}

test_checkHorizontal()


const test_checkVertical = () => {
    const player1 = new Player('Luciano','player1')
    const player2 = new Player('Hudson','player2')
    
    const tests = [
        {
            played: {
                map: [
                    [player1, 0, 0, 0, 0, 0, 0],
                    [player1, 0, 0, 0, 0, 0, 0],
                    [player1, 0, 0, 0, 0, 0, 0],
                    [player1, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0],
                ],
                column: 0,
                row: 0,
                currentPlayer: player1
            },
            expected: true
        },
        {
            played: {
                map: [
                    [0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0],
                    [player1, 0, 0, 0, 0, 0, 0],
                    [player1, 0, 0, 0, 0, 0, 0],
                    [player1, 0, 0, 0, 0, 0, 0],
                    [player1, 0, 0, 0, 0, 0, 0],
                ],
                column: 0,
                row: 2,
                currentPlayer: player1
            },
            expected: true
        },
        {
            played: {
                map: [
                    [0, 0, 0, 0, 0, 0, 0],
                    [0, 0, player1, 0, 0, 0, 0],
                    [0, 0, player1, 0, 0, 0, 0],
                    [0, 0, player1, 0, 0, 0, 0],
                    [0, 0, player1, 0, 0, 0, 0],
                    [0, 0, player2, 0, 0, 0, 0],
                ],
                column: 2,
                row: 1,
                currentPlayer: player1
            },
            expected: true
        },
        {
            played: {
                map: [
                    [0, 0, 0, 0, 0, 0, 0],
                    [0, player1, 0, 0, 0, 0, 0],
                    [0, player1, 0, 0, 0, 0, 0],
                    [0, player1, 0, 0, 0, 0, 0],
                    [0, player2, 0, 0, 0, 0, 0],
                    [0, player1, 0, 0, 0, 0, 0],
                ],
                column: 2,
                row: 1,
                currentPlayer: player1
            },
            expected: false
        }
        
    ]
    const board = new Board(6,7)

    tests.forEach(test => {
        board._map = test.played.map
        board._currentPlayer = test.played.currentPlayer
        console.assert(board.checkVertical(test.played.column, test.played.row) === test.expected, {
            function: "checkVertical",
            test: {...test, got: board.checkVertical(test.played.column, test.played.row)}
        })
    })
}

test_checkVertical()